/*
 * Copyright (C) 2016 Daniel Rossier <daniel.rossier@soo.tech>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef __AVZ_H__
#define __AVZ_H__

#include <soo/uapi/soo.h>

#include <asm/atomic.h>

#define HYPERVISOR_VIRT_START	0xff000000

/*
 * AVZ HYPERCALLS
 */

#define __HYPERVISOR_set_callbacks         0
#define __HYPERVISOR_console_io            2

/**************************************************/

/*
 * Commands to HYPERVISOR_console_io().
 */
#define CONSOLEIO_write_string  0
#define CONSOLEIO_process_char  1

/* Idle domain. */
#define DOMID_IDLE (0x7FFFU)

/* DOMID_SELF is used in certain contexts to refer to oneself. */
#define DOMID_SELF (0x7FF0U)

/* Agency */
#define DOMID_AGENCY	0

extern int hypercall_trampoline(int hcall, long a0, long a2, long a3, long a4);

/*
 * start_info structure
 */

struct start_info {

    int	domID;

    unsigned long nr_pages;     /* Total pages allocated to this domain.  */

    unsigned long hypercall_addr; /* Hypercall vector addr for direct branching without syscall */
    unsigned long fdt_paddr;

    /* Low-level print function mainly for debugging purpose */
    void (*printch)(char c);

    unsigned long store_mfn;    /* MACHINE page number of shared page.    */

    unsigned long nr_pt_frames; /* Number of bootstrap p.t. frames.       */
    unsigned long dom_phys_offset;

    unsigned long pt_vaddr;  /* Virtual address of the page table used when the domain is bootstraping */

};
typedef struct start_info start_info_t;

extern start_info_t *avz_start_info;

#endif /* __AVZ_H__ */

