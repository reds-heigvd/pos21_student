/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * Copyright (C) 2016-2019 Baptiste Delporte <bonel@bonel.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <stdarg.h>
#include <percpu.h>
#include <config.h>
#include <sched.h>
#include <serial.h>
#include <domain.h>
#include <console.h>
#include <errno.h>
#include <softirq.h>
#include <sched-if.h>
#include <memory.h>
#include <heap.h>

#include <asm/processor.h>

#include <soo/uapi/avz.h>
#include <soo/uapi/avz.h>
#include <soo/uapi/debug.h>

/*
 * We don't care of the IDLE domain here...
 * In the domain table, the index 0 and 1 are dedicated to the non-RT and RT agency domains.
 * The indexes 1..MAX_DOMAINS are for the MEs. ME_slotID should correspond to domain ID.
 */
struct domain *domains[MAX_DOMAINS];

struct domain *agency;

int current_domain_id(void)
{
	return current->domain_id;
}

/*
 * Creation of new domain context associated to the agency or a Mobile Entity.
 *
 * @domid is the domain number
 * @partial tells if the domain creation remains partial, without the creation of the vcpu structure which may intervene in a second step
 */
struct domain *domain_create(domid_t domid, int cpu_id)
{
	struct domain *d;

	d = malloc(sizeof(struct domain));
	BUG_ON(!d);

	memset(d, 0, sizeof(struct domain));

	d->domain_id = domid;

	if (!is_idle_domain(d)) {
		d->is_paused_by_controller = 1;
		atomic_inc(&d->pause_count);

	}

	/* Will be used during the context_switch (cf kernel/entry-armv.S */

	arch_domain_create(d, cpu_id);

	d->event_callback = 0;
	d->domcall = 0;

	d->processor = cpu_id;

	if (is_idle_domain(d))
	{
		d->runstate = RUNSTATE_running;
	}
	else
	{
		d->runstate = RUNSTATE_offline;
		set_bit(_VPF_down, &d->pause_flags);
	}

	/* Now, we assign a scheduling policy for this domain */

	if (is_idle_domain(d) && (cpu_id == AGENCY_CPU))
		d->sched = &sched_agency;
	else {

		d->sched = &sched_agency;
		d->need_periodic_timer = true;

	}

	if (sched_init_domain(d, cpu_id) != 0)
		BUG();

	return d;
}


void vcpu_unpause(struct domain *d)
{
	if (atomic_dec_and_test(&d->pause_count))
		vcpu_wake(d);
}

void domain_unpause(struct domain *d)
{
	if (atomic_dec_and_test(&d->pause_count))
		vcpu_wake(d);
}

void domain_unpause_by_systemcontroller(struct domain *d)
{
	if (test_and_clear_bool(d->is_paused_by_controller))
		domain_unpause(d);
}

void context_switch(struct domain *prev, struct domain *next)
{
	local_irq_disable();

	if (!is_idle_domain(current)) {

		local_irq_disable();  /* Again, if the guest re-enables the IRQ */

	}

	if (!is_idle_domain(next)) {

	}

	get_current_addrspace(&prev->addrspace);
	switch_mm(next, &next->addrspace);

	/* Clear running flag /after/ writing context to memory. */
	smp_mb();

	prev->is_running = 0;

	/* Check for migration request /after/ clearing running flag. */
	smp_mb();

	spin_unlock(&prev->sched->sched_data.schedule_lock);

	__switch_to(prev, next);

}

/*
 * Initialize the domain stack used by the hypervisor.
 * This is the H-stack and contains a reference to the domain as the bottom (base) of the stack.
 */
void *setup_dom_stack(struct domain *d) {
	addr_t *domain_stack;

	/* The stack must be aligned at STACK_SIZE bytes so that it is
	 * possible to retrieve the cpu_info structure at the bottom
	 * of the stack with a simple operation on the current stack pointer value.
	 */
	domain_stack = memalign(STACK_SIZE, STACK_SIZE);
	BUG_ON(!domain_stack);

	d->domain_stack = (unsigned long) domain_stack;

	/* Put the address of the domain descriptor at the base of this stack */
	*domain_stack = (addr_t) d;

	/* Reserve the frame which will be restored later */
	domain_stack += (STACK_SIZE - sizeof(cpu_regs_t))/sizeof(addr_t);

	/* Returns the reference to the H-stack frame of this domain */

	return domain_stack;
}

/*
 * Set up the first thread of a domain.
 */
void new_thread(struct domain *d, addr_t start_pc, addr_t fdt_addr, addr_t start_stack, addr_t start_info)
{
	cpu_regs_t *domain_frame;

	domain_frame = (cpu_regs_t *) setup_dom_stack(d);

	if (domain_frame == NULL)
	  panic("Could not set up a new domain stack.n");

	arch_setup_domain_frame(d, domain_frame, fdt_addr, start_info, start_stack, start_pc);
}

static void continue_cpu_idle_loop(void)
{
	while (1) {
		local_irq_disable();

		raise_softirq(SCHEDULE_SOFTIRQ);
		do_softirq();

		ASSERT(local_irq_is_disabled());

		local_irq_enable();

		cpu_do_idle();
	}
}

void startup_cpu_idle_loop(void)
{

	ASSERT(is_idle_domain(current));

	raise_softirq(SCHEDULE_SOFTIRQ);

	continue_cpu_idle_loop();
}

void machine_halt(void)
{
	printk("machine_halt called: spinning....\n");

	while (1);
}

void machine_restart(unsigned int delay_millisecs)
{
	printk("machine_restart called: spinning....\n");

	while (1);
}

